#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Geoffrey::Changelog::JSON' ) || print "Bail out!\n";
}

diag( "Testing Geoffrey::Changelog::JSON $Geoffrey::Changelog::JSON::VERSION, Perl $], $^X" );
